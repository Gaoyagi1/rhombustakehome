# RhombusTakeHome
repo: https://gitlab.com/Gaoyagi1/rhombustakehome

## How to use (skip to step 6 if you have docker, jenkins and it's credentials setup already)
- Assumes you've made a docker/dockerhub and gitlab account
- install homebrew
- using homebrew, install jenkins and docker
- run jenkins and logon and setup your account to it using localhost:8080
    - create global jekins credentials profiles for both your docker and gitlab accounts
- install the docker desktop
- go to your Jenkins dashboard and create a new Multibranch pipeline
    - under branch sources choose git and paste this repo's HTTPS clone url
    - choose your jenkins gitlab credentials and then hit save
- Jenkins should automatically build and run the pipeline
    - go back to dashboard, select the pipeline and the main branch to view the build process
    - you can view the container go live then exit in the docker desktop

#### Problem Statement Scenario:
You are a DevOps consultant in Rhombus Power. We have decided to implement DevSecOps to
develop and deliver our products automatically. Since we are an Agile organization, we follow
Scrum methodology to develop the projects incrementally. You are working with multiple
DevOps Engineers to build a Docker Jenkins Pipeline. During the sprint planning, you agreed to
take the lead on this project and plan the requirements, system configurations, and track the
efficiency. The tasks you are responsible for:  
- [X] Availability of the application and its versions in the GitLab
    - application purpose was never specified? right now it simply prints out "Hello Rhombus"
- Track their versions every time a code is committed to the repository
- [X] Create a Docker Jenkins Pipeline that will create a Docker image from the Dockerfile and host it on Docker Hub
- [X] It should also pull the Docker image and run it as a Docker container
- [X] Build the Docker Jenkins Pipeline to demonstrate the continuous integration and continuous delivery workflow
### Following requirements should be met:
- [X] Document the step-by-step process from the initial installation to the final stage
- [X] Track the versions of the code in the GitLab repository
- [X] Availability of the application in the Docker Hub
- [X] Track the build status of Jenkins for every increment of the project